const HtmlWebpackPlugin = require('html-webpack-plugin')
var path = require('path');
module.exports = {
  entry: path.resolve(__dirname, './src/imp.js'),
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dest'),
    filename: 'expo.bundle.js'
  },
  plugins: [new HtmlWebpackPlugin()],
}