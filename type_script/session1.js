"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
console.log("Chandana Vipparla");
//data types --- number
var first = 12.3; // number   
var second = 0x37CF; // hexadecimal  
var third = 255; // octal  
var fourth = 57; // binary   
console.log(first); // 12  
console.log(second); // 14287  
console.log(third); // 255  
console.log(fourth); // 57 
//---string
var n = "chandana";
var m = "vipparla";
console.log(n + " " + ("" + m));
//---void
function f1() {
    console.log("HELLO!");
}
f1();
//--any
function f2(a, b) {
    console.log(a, b);
}
f2("venky", "vipparla");
//arrays
var arr = ["chandu", "venky", "teju"];
for (var i = 0; i < arr.length; i++) {
    console.log(arr[i]);
}
//array object
var arr1 = new Array(4);
for (var i = 0; i < arr1.length; i++) {
    arr1[i] = i * 2;
}
console.log(arr1);
// var arr2 = new Array(1,"2","ch")
// console.log(arr2);  //error 
//tuple
var t = [10, "Hello", 2, "World"];
console.log("before push " + t);
//push &pop
t.push(3);
console.log("after push length " + t.length);
console.log("before pop " + t);
console.log("popped element is " + t.pop());
console.log("after pop " + t);
var t1;
t1 = ["hi", 8];
console.log(t1);
//functions
function fun2() {
    var msg = fun1(1);
    return (msg);
}
fun2();
function fun1(x1) {
    return x1;
}
console.log(fun1("hi"));
//optional parameters in fun
function disp_details(id, name, mail_id) {
    console.log("ID:", id);
    console.log("Name :", name);
    if (mail_id != undefined)
        console.log("Email Id", mail_id);
}
disp_details(123, "John");
disp_details(111, "mary", "mary@xyz.com");
var customer = {
    firstName: "Tom",
    lastName: "Hanks",
    sayHi: function () { return "Hi there"; }
};
console.log("Customer Object ");
console.log(customer.firstName);
console.log(customer.lastName);
console.log(customer.sayHi());
var employee = {
    firstName: "Jim",
    lastName: "Blakes",
    sayHi: function () { return "Hello!!!"; }
};
console.log("Employee  Object ");
console.log(employee.firstName);
console.log(employee.lastName);
console.log(employee.sayHi());
//--string 
var option = { program: "test1", commandline: "Hello" };
console.log(option.commandline);
//--string array 
option = { program: "test1", commandline: ["Hello", "World"] };
console.log(option.commandline[0]);
console.log(option.commandline[1]);
//--function expression 
option = { program: "test1", commandline: function () { return "**Hello World**"; } };
var fn = option.commandline;
console.log(fn());
