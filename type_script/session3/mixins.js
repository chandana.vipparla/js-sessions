"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var class1 = /** @class */ (function () {
    function class1() {
    }
    class1.prototype.fun1 = function () {
        console.log("in fun1");
    };
    return class1;
}());
var class2 = /** @class */ (function () {
    function class2() {
    }
    class2.prototype.fun2 = function () {
        console.log("in fun2");
    };
    class2.prototype.fun3 = function () {
        console.log("in fun3");
    };
    return class2;
}());
var class3 = /** @class */ (function () {
    function class3() {
        c: "chandana";
    }
    return class3;
}());
function applyMixins1(derivedCtor, baseCtors) {
    baseCtors.forEach(function (baseCtor) {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
            var descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name);
            Object.defineProperty(derivedCtor.prototype, name, descriptor);
        });
    });
}
applyMixins1(class3, [class1, class2]);
var obj = new class3();
obj.fun1();
obj.fun2();
