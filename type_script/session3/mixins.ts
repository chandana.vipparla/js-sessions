 export{}
 class class1 
 {
   fun1()
    {
      console.log("in fun1")
    }
}
class class2
{
  fun2()
  {
    console.log("in fun2")
  }
  fun3()
  {
    console.log("in fun3")
  }
}
class class3 
{
  constructor()
  {
    c:"chandana"
  }
}
interface class3 extends class1, class2 {}
function mix(derivedCtor: any, baseCtors: any[]) 
{
  baseCtors.forEach(baseCtor => 
  {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => 
      {
        let descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name)
        Object.defineProperty(derivedCtor.prototype, name, <PropertyDescriptor & ThisType<any>>descriptor);
      });
  });
}
mix(class3, [class1, class2])
let obj = new class3()
obj.fun1()
obj.fun2()