export{}
//class decorator
@classdecorator
class classA
{
    constructor()
    {
        console.log("chandana");
    }
}
@classdecorator
class classB
{
    @propertydecorator()
    x:string;
    y:string;
    constructor(f:string,z:string)
    {
        this.x=f;
        this.y=z;
        console.log(this.x);
        console.log("in constructor");
    }
    //method decorator
    @methoddecorator()
    fun1()
    {
        a:String;
        console.log("in fun1");
        return  
    } 
    
}
let obj= new classB("class","works");
obj.fun1();
function classdecorator(constructor: Function)
{
    console.log(constructor.name);
    // console.log(constructor.prototype.x);
    constructor.prototype.y="CLASS";
    console.log(constructor);
    console.log(constructor.prototype.y);
    
}
function methoddecorator() {
    return function (target,propertyKey,descriptor) {
      console.log("success")
    };
  }