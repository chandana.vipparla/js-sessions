"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var classA = /** @class */ (function () {
    function classA() {
        console.log("chandana");
    }
    classA = __decorate([
        classdecorator
    ], classA);
    return classA;
}());
var classB = /** @class */ (function () {
    function classB(z, f) {
        this.y = z;
        this.x = f;
        console.log(this.x);
        console.log("in constructor");
    }
    classB.prototype.fun1 = function () {
        a: String;
        console.log("in fun1");
    };
    __decorate([
        methoddecorator()
    ], classB.prototype, "fun1");
    classB = __decorate([
        classdecorator
    ], classB);
    return classB;
}());
var obj = new classB("class", "works");
obj.fun1();
function classdecorator(constructor) {
    console.log(constructor.name);
    // console.log(constructor.prototype.x);
    constructor.prototype.y = "CLASS";
    console.log(constructor);
    console.log(constructor.prototype.y);
}
function methoddecorator() {
    return function (target, propertyKey, descriptor) {
        console.log("success");
    };
}
